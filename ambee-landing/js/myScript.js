
function openCity(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}
function navChange(argument) {
    var nav = document.getElementById('nav')
    var scrollY = window.scrollY
    if (scrollY > 585) {
        nav.className = 'navbar navbar-fixed-top dark-nav-header';
    }
    else{
        nav.className = 'navbar navbar-fixed-top transparent-nav';
    }
}
(function () {  
    changeImage();    
    setInterval(function (argument) {
        changeImage();
    },24000)
}());
function changeImage(argument) {
    var img = document.getElementById('phoneImg')
    // 1
    setTimeout(function() {
        var img = document.getElementById('phoneImg');
        img.src = 'img/screens/iphone2.png';
    }, 3000);
    // 2
    setTimeout(function() {
        var img = document.getElementById('phoneImg');
        img.src = 'img/screens/iphone3.png';
    }, 6000);
    // 3
    setTimeout(function() {
        var img = document.getElementById('phoneImg');
        img.src = 'img/screens/iphone4.png';
    }, 9000);
    // 4
    setTimeout(function() {
        var img = document.getElementById('phoneImg');
        img.src = 'img/screens/iphone5.png';
    }, 12000);
    // 5
    setTimeout(function() {
        var img = document.getElementById('phoneImg');
        img.src = 'img/screens/iphone6.png';
    }, 18000);
    // 6
    setTimeout(function() {
        var img = document.getElementById('phoneImg');
        img.src = 'img/screens/iphone1.png';
    }, 21000);
}